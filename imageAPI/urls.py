"""
imageAPI URLs

This module defines the URL patterns for the imageAPI app.
"""
from django.urls import path

from .views import ImageUploadAndDetectionView

urlpatterns = [
    path('detect/', ImageUploadAndDetectionView.as_view(), name='upload-and-detect'),
]
