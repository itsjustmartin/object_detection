"""
Tests for the image upload and object detection API view.

This module contains test cases to verify the behavior of the API view that handles image uploads
and object detection using YOLOv5. The tests cover various scenarios and edge cases to ensure
the view functions correctly.

Test cases within this module:
- Test the object detection API view with a valid image.
- Add more specific test cases as needed.
"""

from io import BytesIO

from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.urls import reverse
from PIL import Image
from rest_framework import status
from rest_framework.test import APIClient


class ImageUploadAndDetectionViewTest(TestCase):
    """" ImageUploadAndDetectionViewTest """
    def setUp(self):
        self.client = APIClient()

    def test_object_detection(self):
        """
        Test the object detection API view.

        This test case sends a POST request to the object detection API view with a test image.
        It checks if the view returns a valid JSON response with the expected structure.

        The view is expected to perform object detection using the YOLOv5 model.

        Assertions:
        - Response status code should be 200 (OK).
        - Response data should contain 'detected objects' and 'result info'.
        """

        image = Image.new('RGB', (100, 100))
        image_io = BytesIO()
        image.save(image_io, format='JPEG')
        image_file = SimpleUploadedFile(
            "test_image.jpg",
            image_io.getvalue(),
            content_type="image/jpeg"
            )

        url = reverse('upload-and-detect')

        response = self.client.post(url, {'image': image_file})

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue('detected objects' in response.data)
        self.assertTrue('result info' in response.data)

    def test_object_detection_no_file(self):
        """Test for no file provided"""
        url = reverse('upload-and-detect')
        response = self.client.post(url)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertTrue('image' in response.data)
        self.assertEqual(response.data['image'][0], 'No file was submitted.')

    def test_object_detection_invalid_format(self):
        """Test for an invalid image format"""
        invalid_image = SimpleUploadedFile(
            "test_image.txt",
            b"file_content",
            content_type="text/plain"
            )
        url = reverse('upload-and-detect')

        response = self.client.post(url, {'image': invalid_image})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertTrue('image' in response.data)
        self.assertEqual(response.data['image'][0].code,'invalid_extension')
