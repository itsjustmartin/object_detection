"""
Views for handling image uploads and object detection results.

This module defines Django views that manage image uploads and object detection,
"""
import json
import os

import cv2
import numpy as np
import torch
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import ImageUploadSerializer

DEVICE = 'cuda:0' if torch.cuda.is_available() else 'cpu'
MODEL = torch.hub.load('ultralytics/yolov5', 'yolov5s', device=DEVICE)


class ImageUploadAndDetectionView(APIView):
    """
    API view for uploading an image and performing object detection using YOLOv5.

    This view allows clients to upload an image,
        which is then processed to detect objects using the YOLOv5 model.
    The detection results are returned as a JSON response.

    Attributes:
        Image file , Supported file formats: jpg, jpeg, png, tiff, tif, svg, bmp, ico, heic, heif.
    """
    serializer_class = ImageUploadSerializer
    parser_classes = (MultiPartParser,)

    @swagger_auto_schema(
    request_body=ImageUploadSerializer(),
    responses={
        200: openapi.Response(
            description="Successful detection result",
            schema=openapi.Schema(
                type=openapi.TYPE_OBJECT,
                properties={
                    'detected objects': openapi.Schema(
                        type=openapi.TYPE_ARRAY,
                        items=openapi.Schema(
                            type=openapi.TYPE_OBJECT,
                            properties={
                                'class_name': openapi.Schema(type=openapi.TYPE_STRING),
                                'confidence': openapi.Schema(type=openapi.TYPE_STRING),
                                'bounding_box': openapi.Schema(
                                    type=openapi.TYPE_ARRAY,
                                    items=openapi.Schema(type=openapi.TYPE_INTEGER),
                                ),
                            },
                        ),
                    ),
                    'result info': openapi.Schema(type=openapi.TYPE_STRING),
                },
            ),
        ),
        400: "Bad Request - Invalid input data",
    },
)
    def post(self, request):
        """
        Handle HTTP POST requests to perform object detection on an uploaded image.

        Args:
            request (HttpRequest): The HTTP request object containing the uploaded image.

        Returns:
            Response: A JSON response containing the detection results,
                or error messages if the request is invalid.

        Raises:
            N/A

        Example:
            A client can send a POST request to this endpoint with an image file.
                The view will process the image and return the detected objects in JSON format.
        """
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            uploaded_image = serializer.validated_data['image']

            detection_result = perform_object_detection(uploaded_image)

            return Response(detection_result, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def perform_object_detection(uploaded_image):
    """
    Perform object detection on an uploaded image using YOLOv5.

    :param uploaded_image: The file of the uploaded image (InMemoryUploadedFile).
    :return: A list of detected objects with class names,
    confidence scores, and bounding box coordinates.
    """
    img_bytes = uploaded_image.read()
    # pylint: disable=no-member
    img = cv2.imdecode(np.frombuffer(img_bytes, np.uint8), cv2.IMREAD_COLOR)

    results = MODEL(img)

    detected_objects = []
    config_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'config')
    class_names_file = os.path.join(config_dir, 'class_names.json')
    with open(class_names_file, 'r', encoding='utf-8') as file:
        class_names = json.load(file)

    for det in results.pred[0]:
        if det[4] > 0.25:
            label = int(det[5])
            class_name = class_names.get(str(label))
            confidence = float(det[4])
            bounding_box = det[:4].to('cpu').detach().numpy().astype(int).tolist()
            detected_objects.append({
                'class_name': class_name,
                'confidence': f"{int(confidence * 100) }%",
                'bounding_box': bounding_box
            })

    response = {
        "detected objects" : detected_objects,
        "result info" : str(results)
    }
    return response
