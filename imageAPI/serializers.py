"""
Serializers for handling image uploads and detection results.

These serializers define how data is validated and serialized in the 'ImageUploadAndDetectionView'.
"""
from django.core.validators import FileExtensionValidator
from rest_framework import serializers


# pylint: disable=abstract-method
class ImageUploadSerializer(serializers.Serializer):
    """
    Serializer for uploading and detecting objects in an image.

    This serializer is used to upload an image file and receive a detection result.

    Attributes:
        image (FileField): The image file to be uploaded for object detection.
            Supported file formats: jpg, jpeg, png, tiff, tif, svg, bmp, ico, heic, heif.
        detection_result (CharField):
            A read-only field to store the detection result in JSON format.
    """
    image = serializers.FileField(
        write_only=True,
        required=True,
        validators=[
            FileExtensionValidator(
                allowed_extensions=['jpg', 'jpeg', 'png', 'tiff', 'tif',
                                    'svg', 'bmp', 'ico', 'heic', 'heif'],
            ),
        ],
    )
