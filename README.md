# Object Detection API with Django and YOLOv5

This project is an API for performing object detection using the YOLOv5 model with Django REST framework. It allows users to upload an image, which is then processed to detect objects within the image.

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [API Endpoints](#api-endpoints)
- [Swagger Documentation](#swagger-documentation)
- [Contributing](#contributing)
- [License](#license)

## Installation

1. Clone this repository to your local machine:

   ```bash
   git clone https://gitlab.com/itsjustmartin/object_detection.git
   cd object-detection
   ```

2. Create a virtual environment and activate it:

   ```bash
   python -m venv venv
   source venv/bin/activate  # On Windows: venv\Scripts\activate
   ```

3. Install the required dependencies:

   ```bash
   pip install -r requirements.txt
   ```

4. Run the Django development server:

   ```bash
   python manage.py runserver
   ```

## Usage

To use the Object Detection API, you can send a POST request to the appropriate endpoint with an image file. The API will process the image and return the detected objects in JSON format.

## API Endpoints

- `POST /image/detect/`: Endpoint for uploading an image and performing object detection.

  Request Body:
  - `image`: File field for uploading the image.

  Response:
  - JSON response containing the detected objects and result information.

## Swagger Documentation

The API is documented using Swagger, which provides an interactive interface for exploring the API endpoints and making test requests. You can access the Swagger documentation at [http://localhost:8000/swagger/](http://localhost:8000/swagger/) when the server is running.

## Contributing

Contributions are welcome! If you'd like to contribute to this project, please follow these steps:

1. Fork the repository.
2. Create a new branch for your feature or bug fix: `git checkout -b feature-name`.
3. Make your changes and commit them: `git commit -m 'Add new feature'`.
4. Push to your branch: `git push origin feature-name`.
5. Create a pull request to the main repository.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

```

...
